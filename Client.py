import socket
import json
import getpass
import os
import time

class Client:
    def __init__(self, host, port):
        self.HOST = host
        self.PORT = port
        self.ADDR = (self.HOST, self.PORT)
        self.isRunning = True
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect(self.ADDR)
        self.isLogged = False
        self.logged_login = ''
        self.role = 0


    def run(self):
        print("Komenda 'help' wyświetli wszystkie możliwości")

        while self.isRunning:
            command = input("Podaj komendę:")
            time.sleep(1)
            os.system('cls' if os.name == 'nt' else 'clear')
            if command == "stop":
                print("client connection is stopping")
                message = {"command":command}
                self.client.sendall(json.dumps(message).encode('utf-8'))
                break
            if command == "register" or command == "login":
                login = str(input("Podaj login:"))
                password = str(getpass.getpass("Podaj hasło: "))
                message = {
                    "command": command,
                    "username": login,
                    "password": password
                }
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "logout":
                message = {"command":command}
                self.client.sendall(json.dumps(message).encode('utf-8'))
                self.isLogged = False
                self.logged_login = ''
                self.role = 0

            if command == "uptime":
                message = {"command":command}
                self.client.sendall(json.dumps(message).encode('utf-8'))


            if command == "info":
                message = {"command":command}
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "help":
                message = {"command":command}
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "show_inbox":
                if self.role == 1:
                    user = input("Podaj login użytkownika: ")
                else:
                    user = self.logged_login

                message = {"command":command, "user": user}
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "show_message" or command == "delete_message":
                if self.role == 1:
                    user = input("Podaj login użytkownika: ")
                else:
                    user = self.logged_login
                id = input("podaj id wiadomości: ")
                message = {"command": command, "id":id, "user":user}
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "send_message":
                sender = self.logged_login
                recipient = input("Podaj login odbiorcy: ")
                title = input("Podaj tytuł: ")
                text = input("Wpisz treść (max 255 znaków): ")
                message = {"command":command, "from": sender,"recipient":recipient, "title":title, "text":text}
                self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "add_user":
                if self.role == 1:
                    login = input("Podaj login dla użytkownika: ")
                    password = input(f"Podaj hasło dla użytkownika {login}: ")
                    message = {"command": command, "username":login, "password": password}
                    self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "delete_user":
                if self.role == 1:
                    login = input("Podaj login dla użytkownika: ")
                    confirm = input("Jeżeli jesteś pewny wpisz t: ")
                    if confirm == 't':
                        message = {"command": command, "username":login}
                        self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "change_role":
                if self.role == 1:
                    login = input("Podaj login dla użytkownika: ")
                    role = input("Podaj rolę użytkownika 0 - user, 1 - admin: ")
                    message = {"command": command, "username":login, "role": role}
                    self.client.sendall(json.dumps(message).encode('utf-8'))

            if command == "change_password":
                if self.role == 1:
                    login = input("Podaj login dla użytkownika: ")
                    password = input("Podaj hasło dla użytkownika: ")
                    message = {"command": command, "username":login, "password": password}
                    self.client.sendall(json.dumps(message).encode('utf-8'))

            response = self.client.recv(1024)
            if response:
                response_data = json.loads(response.decode('utf-8'))

                if response_data.get("status") == "success":

                    if response_data.get("detail") == "help":
                        print("Dostępne komendy: ")
                        print('\n'.join(x for x in response_data["available_commands"]))

                    if response_data.get("detail") == "uptime":
                        print(f"uptime: {response_data['uptime']}")

                    if response_data.get("detail") == "info":
                        print(f"version: {response_data['version']} \ncreation date: {response_data['creation_date']}")

                    if response_data.get("detail") == "register":
                        print(response_data["message"])

                    if response_data.get("detail") == "login":
                        print(response_data["message"])
                        self.logged_login  = response_data["login"]
                        self.isLogged = True
                        self.role = response_data["role"]

                    if response_data.get("detail") == "logout":
                        print(response_data["message"])

                    if response_data.get("detail") == "email":
                        print(response_data["message"])

                    if response_data.get("detail") == "inbox":
                        print(f"Masz {response_data['unread_count']} nieprzeczytanych wiadomości.")
                        print("Twoja skrzynka: ")
                        print('\n'.join(f"Id wiadomości: {item['id']} \nNadawca: {item['from']} \nTytuł: {item['title']} \n-----------------------------------" for item in response_data["data"]))

                    if response_data.get("detail") == "delete_email":
                        print(response_data["message"])

                    if response_data.get("detail") == "send_email":
                        print(response_data["message"])

                    if response_data.get("detail") == "add_user":
                        print(response_data["message"])

                    if response_data.get("detail") == "delete_user":
                        print(response_data["message"])

                    if response_data.get("detail") == "change_role":
                        print(response_data["message"])

                    if response_data.get("detail") == "change_password":
                        print(response_data["message"])

                elif response_data.get("status") == "error":
                    print(response_data["message"])




        self.client.close()
if __name__ == "__main__":
    HOST = socket.gethostbyname(socket.gethostname())
    PORT = 5050
    client = Client(HOST, PORT)
    client.run()