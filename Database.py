import psycopg2
from config import config

class Database:

    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self):
        try:
            params = config()
            print('Connecting to the PostgreSQL database...')
            self.connection = psycopg2.connect(**params)
            self.cursor = self.connection.cursor()
            print('Connection successful.')
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            raise

    def close(self):
        if self.cursor:
            self.cursor.close()
        if self.connection:
            self.connection.close()
        print('Database connection closed.')

    def execute_query(self, query, params=None):
        self.cursor.execute(query, params)
        self.connection.commit()

    def fetchone(self):
        return self.cursor.fetchone()

    def fetchall(self):
        return self.cursor.fetchall()
