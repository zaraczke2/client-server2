import unittest
import json
import os
import time

from Server import Server

class TestServer(unittest.TestCase):

    def setUp(self):
        self.test_db_path = 'database_test.json'

        with open(self.test_db_path, 'w') as file:
            json.dump({}, file)

        self.server = Server(start_server=False)

        self.server.database_path = self.test_db_path

        self.sample_user = {
            "username": "testuser",
            "password": "testpass"
        }
        self.bad_sample_user_login = {
            "username": "testuser1",
            "password": "testpass"
        }

        self.bad_sample_user_password = {
            "username": "testuser",
            "password": "testpass1"
        }

        self.sample_recipient = {
            "username": "recipientuser",
            "password": "recipientpass"
        }

        self.message_data = {
            "from": "testuser",
            "title": "Test Message",
            "text": "This is a test message.",
            "recipient": self.sample_recipient["username"]
        }
        self.message_data_with_bad_recipient = {
            "from": "testuser",
            "title": "Test Message",
            "text": "This is a test message.",
            "recipient": "abcdefgh"
        }
        self.message_data_bad_length = {
            "from": "testuser",
            "title": "Test Message",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin sapien justo.",
            "recipient": self.sample_recipient["username"]
        }
        self.message_data_for_delete = {
            "id": 1,
            "user": self.sample_recipient["username"]
        }

    def tearDown(self):
        self.server.shutdown()
        if os.path.exists(self.test_db_path):
            os.remove(self.test_db_path)

    def test_register(self):
        response = self.server.register(self.sample_user)
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "Registration successful")

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)

        self.assertEqual(len(users), 1)
        self.assertIn(self.sample_user["username"], users)

    def test_bad_register_with_exist_user(self):
        self.server.register(self.sample_user)
        response = self.server.register(self.sample_user)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Username already exists")

    def test_bad_register_with_logged(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        response = self.server.register(self.sample_user)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "You are already logged")

    def test_login(self):
        self.server.register(self.sample_user)
        response = self.server.login(self.sample_user)
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "Login successful!")
        self.assertEqual(response["detail"], "login")
        self.assertEqual(response["login"], self.sample_user["username"])

    def test_bad_login_with_invalid_data_login(self):
        self.server.register(self.sample_user)
        response = self.server.login(self.bad_sample_user_login)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Invalid username or password")

    def test_bad_login_with_invalid_data_password(self):
        self.server.register(self.sample_user)
        response = self.server.login(self.bad_sample_user_password)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Invalid username or password")

    def test_bad_login_with_logged_in(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        response = self.server.login(self.sample_user)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "You are already logged")

    def test_logout(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        response = self.server.logout()
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "Logout successful!")

    def test_bad_logout(self):
        response = self.server.logout()
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "You haven't logged in yet!")

    def test_help_without_login(self):
        response = self.server.help()
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["available_commands"], ["uptime", "info", "stop", "help","register", "login"])

    def test_help_user_logged(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        response = self.server.help()
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["available_commands"], ["uptime", "info", "stop", "help", "send_message", "show_inbox", "show_message", "delete_message", "logout"])

    def test_help_user_admin_logged(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1
        response = self.server.help()
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["available_commands"], ["uptime", "info", "stop", "help", "send_message", "show_inbox", "show_message", "delete_message", "logout", "add_user", "delete_user", "change_role", "change_password"])


    def test_uptime(self):
        response = self.server.uptime()
        uptime_seconds = time.time() - self.server.start_time
        self.assertEqual(response["status"], "success")
        self.assertAlmostEqual(response["uptime"], round(uptime_seconds, 2))

    def test_info(self):
        response = self.server.info()
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["detail"], "info")
        self.assertEqual(response["version"], self.server.VERSION)
        self.assertEqual(response["creation_date"], self.server.CREATION_DATE)

    def test_send_message(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        response = self.server.send_message(self.message_data)
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "Message sent successfully")

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)
        messages = users[self.sample_recipient["username"]]["messages"]
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0]["title"], "Test Message")

    def test_send_message_too_long(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        response = self.server.send_message(self.message_data_bad_length)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Message too long!")

    def test_send_message_too_many(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        for i in range(5):
            self.server.send_message(self.message_data)

        response = self.server.send_message(self.message_data)

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "The user has too many unread messages! Try again later.")

    def test_send_message_unknown_recipient(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        response = self.server.send_message(self.message_data_with_bad_recipient)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "User not found")

    def test_send_message_without_login(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)

        response = self.server.send_message(self.message_data)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Please log in!")

    def test_delete_message(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()
        self.server.login(self.sample_recipient)
        response = self.server.delete_message(self.message_data_for_delete)

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["detail"], "delete_email")

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)
        messages = users[self.sample_recipient["username"]]["messages"]
        self.assertEqual(len(messages), 0)

    def test_show_inbox(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()
        self.server.login(self.sample_recipient)
        response = self.server.show_inbox({"user" : self.sample_recipient["username"]})

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "If you want to read the message, enter the show_message command")

    def test_show_inbox_bad(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()

        response = self.server.show_inbox({"user" : self.sample_recipient["username"]})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Please log in!")

    def test_show_message(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()
        self.server.login(self.sample_recipient)

        data = {"id":1, "user":self.sample_recipient["username"]}

        response = self.server.show_message(data)

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], f"Nadawca: {self.sample_user['username']}\nWiadomość: {self.message_data['text']}" )

    def test_show_message_with_bad_id(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()
        self.server.login(self.sample_recipient)

        data = {"id":99, "user":self.sample_recipient["username"]}

        response = self.server.show_message(data)

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Invalid message ID" )

    def test_show_message_without_login(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.send_message(self.message_data)
        self.server.logout()

        data = {"id":1, "user":self.sample_recipient["username"]}

        response = self.server.show_message(data)

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Please log in!" )

    def test_add_user(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.add_user(self.sample_recipient)
        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], "The user has been added!" )

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)

        self.assertEqual(len(users), 2)
        self.assertIn(self.sample_recipient["username"], users)

    def test_add_user_with_user_exist(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.add_user(self.sample_recipient)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Username already exists" )

    def test_add_user_without_permissions(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)

        response = self.server.add_user(self.sample_recipient)
        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"], "Insufficient permissions")

    def test_delete_user(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.delete_user({"username":self.sample_recipient["username"]})

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"], f"User {self.sample_recipient['username']} has been deleted successfully" )

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)
        self.assertEqual(len(users), 1)

    def test_delete_user_non_exist_user(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.delete_user({"username":self.sample_recipient["username"]})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Username does not exist" )

    def test_delete_user_current(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.delete_user({"username":self.sample_user["username"]})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Cannot delete the currently logged-in user" )

    def test_delete_user_without_permissions(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)

        response = self.server.delete_user({"username":self.sample_user["username"]})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Insufficient permissions" )

    def test_change_role(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_role({"username":self.sample_recipient["username"],"role": 1})

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"],  f"{self.sample_recipient['username']}'s role has been changed successfully" )

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)
        self.assertEqual(users[f"{self.sample_recipient['username']}"]["role"], 1)

    def test_change_role_user_not_exist(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_role({"username":self.sample_recipient["username"],"role": 1})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Username does not exist")

    def test_change_role_user_logged(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_role({"username":self.sample_user["username"],"role": 0})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Cannot change role the currently logged-in user")

    def test_change_role_user_without_permission(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        response = self.server.change_role({"username":self.sample_recipient["username"],"role": 0})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Insufficient permissions")

    def test_change_password(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_password({"username":self.sample_recipient["username"],"password": "12345"})

        self.assertEqual(response["status"], "success")
        self.assertEqual(response["message"],  f"{self.sample_recipient['username']}'s password has been changed successfully" )

        with open(self.test_db_path, 'r') as f:
            users = json.load(f)
        self.assertEqual(users[f"{self.sample_recipient['username']}"]["password"], "12345")

    def test_change_password_user_not_exist(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_password({"username":self.sample_recipient["username"],"password": "12345"})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Username does not exist")

    def test_change_password_user_logged(self):
        self.server.register(self.sample_user)
        self.server.login(self.sample_user)
        self.server.role = 1

        response = self.server.change_password({"username":self.sample_user["username"],"password": "12345"})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Cannot change password the currently logged-in user")

    def test_change_password_user_without_permission(self):
        self.server.register(self.sample_user)
        self.server.register(self.sample_recipient)
        self.server.login(self.sample_user)

        response = self.server.change_role({"username":self.sample_recipient["username"],"password": "12345"})

        self.assertEqual(response["status"], "error")
        self.assertEqual(response["message"],  "Insufficient permissions")

if __name__ == "__main__":
    unittest.main()