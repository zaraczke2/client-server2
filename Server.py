import socket
import datetime
import json
import time
from Database import Database

class Server:
    def __init__(self, start_server=True):
        self.PORT = 5050
        self.SERVER = socket.gethostbyname(socket.gethostname())
        self.ADDR = (self.SERVER, self.PORT)
        self.VERSION = "0.1.0"
        self.CREATION_DATE = f"{datetime.date.today()}"
        self.isRunning = True
        self.start_time = time.time()
        self.serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # self.database_path = 'database.json'
        self.database = Database()
        self.connect_to_database()
        self.database.initialize_tables()

        if start_server:
            self.initialize_server()

        self.commands = ["uptime", "info", "stop", "help","register", "login"]
        self.commands_for_user = self.commands + ["send_message","show_inbox","show_message","delete_message","logout"]
        self.commands_for_user = [cmd for cmd in self.commands_for_user if cmd not in ["login", "register"]]
        self.commands_for_admin = self.commands_for_user + ["add_user","delete_user", "change_role", "change_password"]
        self.isLogged = False
        self.logged_login = ''
        self.role = 0

    def initialize_server(self):
        self.serv.bind(self.ADDR)
        self.serv.listen(2)
        print(f"Server is running on {self.SERVER}:{self.PORT}")

    def connect_to_database(self):
        self.database.connect()

    def shutdown(self):
        self.serv.close()
        self.isRunning = False
        self.database.close()


    def help(self):
        if self.isLogged:
            if self.role == 1:
                return{"status":"success","detail":"help","available_commands": self.commands_for_admin}
            return{"status":"success","detail":"help","available_commands": self.commands_for_user}

        return {"status":"success","detail":"help","available_commands": self.commands}

    def uptime(self):
        uptime_seconds = time.time() - self.start_time
        return {"status":"success","detail":"uptime", "uptime": round(uptime_seconds, 2)}

    def info(self):
        return {"status":"success","detail":"info", "version": self.VERSION, "creation_date": self.CREATION_DATE}

    def register(self, client_data):
        if not self.isLogged:
            # with open(self.database_path, 'r') as file:
            #     users = json.load(file)

            username = client_data.get("username")

            self.database.execute_query("SELECT COUNT(*) FROM users WHERE username = %s",(username,))
            user_count = self.database.fetchone()[0]

            if user_count > 0:
                return {"status":"error", "message":"username already exist"}


            # if username in users:
            #     return {"status":"error","message":"Username already exists"}

            user = self.new_user(client_data)

            self.database.execute_query(
                "INSERT INTO users (username, password, role) VALUES (%s,%s,%s)",
                (user["username"], user["password"], user["role"])
            )
            # users.update(user)
            # with open(self.database_path, 'w') as file:
            #     json.dump(users, file, indent=4)

            return {"status":"success","detail":"register","message": "Registration successful"}
        else:
            return {"status":"error","message":"You are already logged"}

    def new_user(self, client_data):
        username = client_data.get("username")
        password = client_data.get("password")
        return {"username":"username","password": password, "role": 0,}

    def login(self, client_data):
        if not self.isLogged:
            with open(self.database_path, 'r') as file:
                users = json.load(file)

            username = client_data.get("username")
            password = client_data.get("password")

            if username not in users:
                return {"status":"error","message": "Invalid username or password"}
            stored_password = users[username]["password"]
            if password != stored_password:
                return {"status":"error","message": "Invalid username or password"}

            self.isLogged = True
            self.logged_login = username
            self.role = users[username]["role"]

            return {"status":"success","detail":"login","message": "Login successful!", "login":username, "role":self.role}
        else:
            return {"status":"error","message": "You are already logged"}

    def logout(self):
        if not self.isLogged:
            return {"status":"error","message": "You haven't logged in yet!"}
        elif self.isLogged:
            self.isLogged = False
            self.logged_login = ''
            self.role = 0
            return {"status":"success", "detail":"logout", "message":"Logout successful!"}

    def show_inbox(self, client_data):
        if self.isLogged:
            with open(self.database_path, 'r') as file:
                database = json.load(file)
                user = client_data["user"]
                messages = database[user]["messages"]
                filtered_messages = [
                    {
                        "id": message["id"],
                        "title": message["title"],
                        "from": message["from"]
                    }
                    for message in messages
                ]
                unread_count = sum(1 for message in messages if message["read_off"] == 0)
            return {"status":"success","detail":"inbox","message": "If you want to read the message, enter the show_message command", "data":filtered_messages, "unread_count": unread_count}
        else:
            return {"status":"error","message": "Please log in!"}

    def show_message(self, client_data):
        if self.isLogged:
            with open(self.database_path, 'r') as file:
                database = json.load(file)
                message_id = int(client_data.get("id"))
                user = client_data["user"]
            for message_obj in database[user]["messages"]:
                if message_obj.get("id") == message_id:
                    message = message_obj.get("message")
                    sender = message_obj.get("from")

                    message_obj["read_off"] = 1

                    with open(self.database_path, 'w') as file:
                        json.dump(database, file, indent=4)

                    return {"status": "success", "detail":"email", "message": f"Nadawca: {sender}\nWiadomość: {message}"}

            return {"status": "error", "message": "Invalid message ID"}
        else:
            return {"status": "error", "message": "Please log in!"}

    def delete_message(self, client_data):
        if self.isLogged:
            with open(self.database_path, 'r') as file:
                database = json.load(file)
                message_id = int(client_data.get("id"))
                user = client_data["user"]

            if user not in database:
                return {"status": "error", "message": "User not found"}

            messages = database[user]["messages"]
            message_found = False
            sender = ''
            for i, message_obj in enumerate(messages):
                if message_obj.get("id") == message_id:
                    message_found = True
                    sender = message_obj.get("from")

                    del messages[i]
                    break

            with open(self.database_path, 'w') as file:
                json.dump(database, file, indent=4)

            if message_found:
                return {"status": "success", "detail": "delete_email", "message": f"Message from {sender} deleted successfully"}
            else:
                return {"status": "error", "message": "Invalid message ID"}
        else:
            return {"status": "error", "message": "Please log in!"}

    def send_message(self, client_data):
        if self.isLogged:
            with open(self.database_path, 'r') as file:
                database = json.load(file)
                sender = client_data.get("from")
                title = client_data.get("title")
                text = client_data.get("text")
                recipient = client_data.get("recipient")

                if recipient not in database:
                    return {"status": "error", "message": "User not found"}

                unread_count = sum(1 for message in database[recipient]["messages"] if message["read_off"] == 0)
                if len(text) > 254:
                    return {"status": "error", "message": "Message too long!"}
                else:
                    if unread_count > 4:
                        return {"status": "error", "message": "The user has too many unread messages! Try again later."}
                    else:
                        if database[recipient]["messages"]:
                            new_id = max(message["id"] for message in database[recipient]["messages"]) + 1
                        else:
                            new_id = 1

                        new_message = {
                            "id": new_id,
                            "title": title,
                            "from": sender,
                            "message": text,
                            "read_off": 0
                        }

                        database[recipient]["messages"].append(new_message)

                    with open(self.database_path, 'w') as file:
                        json.dump(database, file, indent=4)

                    return {"status": "success", "detail":"send_email","message": "Message sent successfully"}
        else:
            return {"status": "error", "message": "Please log in!"}


    def add_user(self, client_data):
        if self.role == 1:
            with open(self.database_path, 'r') as file:
                users = json.load(file)

            username = client_data.get("username")

            if username in users:
                return {"status":"error","message":"Username already exists"}

            user = self.new_user(client_data)
            users.update(user)

            with open(self.database_path, 'w') as file:
                json.dump(users, file, indent=4)

            return {"status":"success","detail":"register","message": "The user has been added!"}
        else:
            return {"status": "error", "message": "Insufficient permissions"}



    def delete_user(self, client_data):
        if self.role == 1:
            with open(self.database_path, 'r') as file:
                users = json.load(file)

            username = client_data.get("username")

            if username not in users:
                return {"status": "error", "message": "Username does not exist"}

            if username == self.logged_login:
                return {"status": "error", "message": "Cannot delete the currently logged-in user"}

            del users[username]

            with open(self.database_path, 'w') as file:
                json.dump(users, file, indent=4)

            return {"status": "success", "message": f"User {username} has been deleted successfully"}
        else:
            return {"status": "error", "message": "Insufficient permissions"}

    def change_role(self, client_data):
        if self.role == 1:
            with open(self.database_path, 'r') as file:
                users = json.load(file)

            username = client_data.get("username")

            if username not in users:
                return {"status": "error", "message": "Username does not exist"}

            if username == self.logged_login:
                return {"status": "error", "message": "Cannot change role the currently logged-in user"}

            users[username]["role"] = client_data.get("role")

            with open(self.database_path, 'w') as file:
                json.dump(users, file, indent=4)

            return {"status": "success", "message": f"{username}'s role has been changed successfully"}
        else:
            return {"status": "error", "message": "Insufficient permissions"}

    def change_password(self, client_data):
        if self.role == 1:
            with open(self.database_path, 'r') as file:
                users = json.load(file)

            username = client_data.get("username")

            if username not in users:
                return {"status": "error", "message": "Username does not exist"}

            if username == self.logged_login:
                return {"status": "error", "message": "Cannot change password the currently logged-in user"}

            users[username]["password"] = client_data.get("password")

            with open(self.database_path, 'w') as file:
                json.dump(users, file, indent=4)

            return {"status": "success", "message": f"{username}'s password has been changed successfully"}
        else:
            return {"status": "error", "message": "Insufficient permissions"}

    def run(self):
        while self.isRunning:
            client, address = self.serv.accept()
            print(f"Uzyskano połączenie od {address[0]}:{address[1]}")

            while True:
                data = client.recv(1024)
                if not data:
                    self.isRunning = False
                    break

                received_data = json.loads(data.decode('utf-8'))


                command = received_data.get("command")
                response = {}

                if command == "help":
                    response = self.help()
                elif command == "uptime":
                    response = self.uptime()
                elif command == "info":
                    response = self.info()
                elif command == "stop":
                    self.isRunning = False
                    print("Server is stopping.")
                    response = {"message": "Server is stopping."}
                elif command == "register":
                    response = self.register(received_data)
                elif command == "login":
                    response = self.login(received_data)
                elif command == "logout":
                    response = self.logout()
                elif command == "show_inbox":
                    response = self.show_inbox(received_data)
                elif command == "show_message":
                    response = self.show_message(received_data)
                elif command == "delete_message":
                    response = self.delete_message(received_data)
                elif command == "send_message":
                    response = self.send_message(received_data)
                elif command == "add_user":
                    response = self.add_user(received_data)
                elif command == "delete_user":
                    response = self.delete_user(received_data)
                elif command == "change_role":
                    response = self.change_role(received_data)
                elif command == "change_password":
                    response = self.change_password(received_data)

                client.sendall(json.dumps(response).encode('utf-8'))

                if command == "stop":
                    break


if __name__ == "__main__":
    server = Server()
    server.run()